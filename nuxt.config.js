export default {
  mode: 'universal',
  srcDir: 'src/',
  router: {
    prefetchLinks: false
  },
  buildModules: [
    '@nuxtjs/tailwindcss',
    "@nuxtjs/svg",
  ],
  build: {
    postcss: {
      preset: {
        features: {
          "focus-within-pseudo-class": false
        }
      },
      plugins: {
        tailwindcss: './src/tailwind.config.js'
      }
    },
    extractCSS: true,
    extend(config, ctx) {
    },
    html: {
      minify: {
        collapseBooleanAttributes: true,
        decodeEntities: true,
        minifyCSS: true,
        minifyJS: true,
        processConditionalComments: true,
        removeEmptyAttributes: true,
        removeRedundantAttributes: true,
        trimCustomFragments: true,
        useShortDoctype: true
      }
    },
    optimization: {
      splitChunks: {
        chunks: 'async',
      }
    },
    analyzerMode: 'static',
    splitChunks: {
      pages: false,
      vendor: false,
      commons: false,
      runtime: false,
      layouts: false,
      components: false,
      plugins: false
    },
  },
  purgeCSS: {
    enabled: false
  },
  plugins: [
    '@/plugins/brands.js',
    '@/plugins/icons.js',
    // { src: '~/plugins/fb-chat.js', ssr: false }
  ]
}