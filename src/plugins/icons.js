import Vue from 'vue'
import Backend from '~/assets/svg/Backend.svg?inline'
import Teacher from '~/assets/svg/Teacher.svg?inline'
import FrontEnd from '~/assets/svg/FrontEnd.svg?inline'
import Server from '~/assets/svg/Server.svg?inline'
import Check from '~/assets/svg/Check.svg?inline'
import Heart from '~/assets/svg/Heart.svg?inline'

Vue.component('IconHeart', Heart)
Vue.component('IconBackend', Backend)
Vue.component('IconTeacher', Teacher)
Vue.component('IconFrontEnd', FrontEnd)
Vue.component('IconServer', Server)
Vue.component('IconCheck', Check)