import Vue from 'vue'
import Aws from '~/assets/svg/AwsBrands.svg?inline'
import Css from '~/assets/svg/CssBrands.svg?inline'
import Django from '~/assets/svg/DjangoBrands.svg?inline'
import Docker from '~/assets/svg/DockerBrands.svg?inline'
import ExpressGateway from '~/assets/svg/ExpressGatewayBrands.svg?inline'
import ExpressJs from '~/assets/svg/ExpressJsBrands.svg?inline'
import Facebook from '~/assets/svg/FacebookBrands.svg?inline'
import Flask from '~/assets/svg/FlaskBrands.svg?inline'
import Github from '~/assets/svg/GithubBrands.svg?inline'
import Html from '~/assets/svg/HtmlBrands.svg?inline'
import Js from '~/assets/svg/JsBrands.svg?inline'
import Laravel from '~/assets/svg/LaravelBrands.svg?inline'
import LinkedIn from '~/assets/svg/LinkedInBrands.svg?inline'
import Node from '~/assets/svg/NodeBrands.svg?inline'
import NuxtJs from '~/assets/svg/NuxtJsBrands.svg?inline'
import OctoberCms from '~/assets/svg/OctobercmsBrands.svg?inline'
import Php from '~/assets/svg/PhpBrands.svg?inline'
import Python from '~/assets/svg/PythonBrands.svg?inline'
import Symfony from '~/assets/svg/SymfonyBrands.svg?inline'
import Twitch from '~/assets/svg/TwitchBrands.svg?inline'
import Vuejs from '~/assets/svg/VuejsBrands.svg?inline'
import Wordpress from '~/assets/svg/WordpressBrands.svg?inline'
import MongoDB from '~/assets/svg/MongoDbBrands.svg?inline'
import MySql from '~/assets/svg/MySqlBrands.svg?inline'
import PostgreSql from '~/assets/svg/PostgreSqlBrands.svg?inline'
import Ydb from '~/assets/svg/YdbBrands.svg?inline'
import Youtube from '~/assets/svg/YoutubeBrands.svg?inline'
import OrientDb from '~/assets/svg/OrientDbBrands.svg?inline'
import ArchOs from '~/assets/svg/ArchOsBrands.svg?inline'
import CentOs from '~/assets/svg/CentOsBrands.svg?inline'
import Ubuntu from '~/assets/svg/UbuntuBrands.svg?inline'
import Debian from '~/assets/svg/DebianBrands.svg?inline'
import Gitlab from '~/assets/svg/GitlabBrands.svg?inline'
import Git from '~/assets/svg/GitBrands.svg?inline'
import TailwindCss from '~/assets/svg/TailwindCss.svg?inline'
import Netlify from '~/assets/svg/Netlify.svg?inline'
import Webpack from '~/assets/svg/Webpack.svg?inline'
import Logo from '~/components/Logo.vue'


import Brand from '~/components/Brand.vue'

Vue.component('Brand', Brand)
Vue.component('BrandWebpack', Webpack)
Vue.component('BrandNetlify', Netlify)
Vue.component('BrandTailwindCss', TailwindCss)
Vue.component('BrandGit', Git)
Vue.component('BrandGitlab', Gitlab)
Vue.component('BrandArchOs', ArchOs)
Vue.component('BrandCentOs', CentOs)
Vue.component('BrandUbuntu', Ubuntu)
Vue.component('BrandDebian', Debian)
Vue.component('BrandOrientDb', OrientDb)
Vue.component('BrandMongoDb', MongoDB)
Vue.component('BrandMySql', MySql)
Vue.component('BrandPgSql', PostgreSql)
Vue.component('BrandYdb', Ydb)
Vue.component('BrandAws', Aws)
Vue.component('BrandCss', Css)
Vue.component('BrandDjango', Django)
Vue.component('BrandDocker', Docker)
Vue.component('BrandExpressGateway', ExpressGateway)
Vue.component('BrandExpressJs', ExpressJs)
Vue.component('BrandFacebook', Facebook)
Vue.component('BrandFlask', Flask)
Vue.component('BrandGithub', Github)
Vue.component('BrandHtml', Html)
Vue.component('BrandJs', Js)
Vue.component('BrandLaravel', Laravel)
Vue.component('BrandLinkedIn', LinkedIn)
Vue.component('BrandNode', Node)
Vue.component('BrandNuxtJs', NuxtJs)
Vue.component('BrandOctoberCms', OctoberCms)
Vue.component('BrandPhp', Php)
Vue.component('BrandPython', Python)
Vue.component('BrandSymfony', Symfony)
Vue.component('BrandTwitch', Twitch)
Vue.component('BrandVueJs', Vuejs)
Vue.component('BrandWordpress', Wordpress)
Vue.component('BrandYoutube', Youtube)
Vue.component('Logo', Logo)