import Vue from 'vue'
import VueFbCustomerChat from 'vue-fb-customer-chat'

Vue.use(VueFbCustomerChat, {
  page_id: process.env.NUXT_ENV_FB_PAGE_ID,
  theme_color: '#2f855a',
  locale: 'en_US',
  logged_in_greeting: 'Hello There!',
})