FROM node AS build

RUN mkdir /app
WORKDIR /app
COPY package.json /app
COPY package-lock.json /app

RUN npm install
RUN npm run generate

FROM nginx:stable

COPY --from=build /app/dist /usr/share/nginx/html